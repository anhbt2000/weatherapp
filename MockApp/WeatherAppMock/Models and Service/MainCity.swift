//
//  MainCity.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//


import Foundation

struct City: Codable, Identifiable, Hashable {
    var id: String{
        return name!
    }
    let name: String?
    let lon: String?
    let lat: String?
    let country: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case lon = "lon"
        case lat = "lat"
        case country = "country"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        lon = try container.decodeIfPresent(String.self, forKey: .lon)
        lat = try container.decodeIfPresent(String.self, forKey: .lat)
        country = try container.decodeIfPresent(String.self, forKey: .country)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(lon, forKey: .lon)
        try container.encodeIfPresent(lat, forKey: .lat)
        try container.encodeIfPresent(country, forKey: .country)
    }
}

struct CityResponse: Codable {
    var cities_list: [City]?
    
    enum CodingKeys: String, CodingKey {
        case cities_list = "cities_list"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        cities_list = try container.decodeArrayIfPresent(City.self, forKey: .cities_list)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeArrayIfPresent(cities_list, forKey: .cities_list)

    }
}
