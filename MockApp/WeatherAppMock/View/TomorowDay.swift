//
//  TomorowDay.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//

import SwiftUI

struct TomorowDay: View {
    @State var data: [Weather]
    @State var listDataHour: [HourlyWeather]
    @State var tomorow: EightDayList
    @StateObject var weatherViewModel = WeatherViewModel()
    let taskDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm dd 'Tháng' MM"
        return formatter
    }()
    var body: some View {
        ScrollView {
            VStack {
                Text("\(Date().dateAfterNumberOfdays(days: 1), formatter: taskDateFormat)")
                    .foregroundColor(.white)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)
                HStack(alignment: .center, spacing: 74) {
                    if listDataHour.count > 0 {
                        ForEach(data, id: \.id) { item in
                            ItemCellView(item: item, listDataHour: tomorow, index: 1)
                        }
                    }
                }
                
                Rectangle()
                    .fill(.white)
                    .frame(width: UIScreen.main.bounds.width - 32, height: 2, alignment: .center)
                    .padding(.top, 10)
                
                Text("Hằng giờ")
                    .foregroundColor(.white)
                    .fontWeight(.heavy)
                    .font(.system(size: 18))
                    .padding()
                    .listRowInsets(EdgeInsets())
                    .frame(width: UIScreen.main.bounds.width, height: 44, alignment: .center)
                    .background(Color(red: 0.01, green: 0.748, blue: 0.652))
                
                ForEach(0..<listDataHour.count) { row in
                    HStack(alignment: .center) {
                        Text("\(listDataHour[row].hour)h")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.leading, 18)
                        Spacer()
                        Image("\(String().setImage(image: self.listDataHour[row].weather.first?.icon ?? ""))")
                        Spacer()
                        Image("ic_humidity")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text("\(listDataHour[row].main.humidity)%")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                        Spacer()
                        Image("ic_temp")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text("\(listDataHour[row].main.temp.toCelsiusTemp(kelvinTemp: listDataHour[row].main.temp))")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.trailing, 18)
                    }
                }
                .padding()
                .listRowInsets(EdgeInsets())
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .onAppear() {
                weatherViewModel.getAPI()
            }
            .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            .listStyle(PlainListStyle())
        }  .background(Color(red: 0.01, green: 0.748, blue: 0.652))
    }
}
