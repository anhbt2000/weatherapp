//
//  WeatherService.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import Foundation
 
class WeatherService {
    
    func getListDataCity() -> [City] {
        let fileName: String = "list_cities"
        var result: [City] = [City]()
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            let decoder = JSONDecoder()
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let response = try? decoder.decode(CityResponse.self, from: data)
                result = response?.cities_list ?? [City]()
            } catch {
                
            }
        }
        return result
    }
    
    func getWeather(latitude: String, longitude: String, completion: @escaping (ListWeather?) -> ()) {
        guard let url = Constant.urlWeatherByCoordinate(lat: latitude, lon: longitude) else {
            completion(nil)
            return
        }
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: url) { data, respone, error in
            if error == nil && data != nil {
                let decoder = JSONDecoder()
                do {
                    let listWeather = try decoder.decode(ListWeather.self, from: data!)
                    completion(listWeather)
                } catch {
                    print("Error in Json parsing")
                }
                
            }
        }
        // Make the API Call
        dataTask.resume()
    }
    
    func getWeatherHourly(latitude: String, longitude: String, completion: @escaping (ListWeatherHourly?) -> ()) {
        guard let url = Constant.urlWeatherHourlyByCoordinate(lat: latitude, lon: longitude) else {
            completion(nil)
            return
        }
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: url) { data, respone, error in
            if error == nil && data != nil {
                let decoder = JSONDecoder()
                do {
                    let listWeatherHourly = try decoder.decode(ListWeatherHourly.self, from: data!)
                    completion(listWeatherHourly)
                } catch {
                    print("Error in Json parsing")
                    let fileName: String = "hourly_weather"
                    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
                        do {
                            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                            let response = try? decoder.decode(ListWeatherHourly.self, from: data)
                            completion(response)
                        } catch {
                            print("Error in Json parsing")
                        }
                    }
                }
                
            }
        }
        // Make the API Call
        dataTask.resume()
    }
    
    func get8WeatherDay(latitude: String, longitude: String, completion: @escaping (ListEightDayWeather?) -> ()) {
        guard let url = Constant.url8WeatherDayByCoordinate(lat: latitude, lon: longitude) else {
            completion(nil)
            return
        }
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: url) { data, respone, error in
            if error == nil && data != nil {
                let decoder = JSONDecoder()
                do {
                    let listEightDayWeather = try decoder.decode(ListEightDayWeather.self, from: data!)
                    completion(listEightDayWeather)
                } catch {
                    print("Error in Json parsing")
                    let fileName: String = "eight_weather"
                    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
                        do {
                            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                            let response = try? decoder.decode(ListEightDayWeather.self, from: data)
                            completion(response)
                        } catch {
                            print("Error in Json parsing")
                        }
                    }
                }
                
            }
        }
        // Make the API Call
        dataTask.resume()
    }
}
