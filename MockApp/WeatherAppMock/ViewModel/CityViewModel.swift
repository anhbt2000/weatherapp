//
//  CityViewModel.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//


import Foundation
import Combine

class CityViewModel: ObservableObject {
    private var weatherService: WeatherService!
    
    @Published var cities = [City]()
    
    init() {
        self.weatherService = WeatherService()
    }
    
    var listCities: [City] {
        return self.cities
    }
    
    func getListCities() {
        self.cities = weatherService.getListDataCity()
    }
}

