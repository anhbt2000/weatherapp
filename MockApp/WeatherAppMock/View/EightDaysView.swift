//
//  EightDaysView.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//

import SwiftUI

struct EightDaysView: View {
    @State var listData: [EightDayList]
    @StateObject var weatherViewModel = WeatherViewModel()
    
    let taskDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    var body: some View {
        List {
            ForEach(0..<self.listData.count) { row in
                HStack(alignment: .center) {
                    VStack(alignment: .leading) {
                        Text(row == 0 ? "Hôm nay" :"\(Date().dateAfterNumberOfdays(days: row), formatter: taskDateFormat)")
                            .foregroundColor(.white)
                            .font(.system(size: 16))
                            .fontWeight(.bold)
                        Text(self.listData[row].weather.first?.weatherDescription ?? "")
                            .foregroundColor(.white)
                            .font(.system(size: 14))
                            .fontWeight(.bold)
                    }.padding(.leading, 18)
                    
                    Spacer()
                    
                    Image("\(String().setImage(image: self.listData[row].weather.first?.icon ?? ""))")
                        .frame(width: 20, height: 20, alignment: .center)
                        .padding(.trailing, 27)
                    
                    Text("\(self.listData[row].humidity)%")
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding(.trailing, 10)
                    
                    VStack(alignment: .center) {
                        Text("\(self.listData[row].temp.max.toCelsiusTemp(kelvinTemp: self.listData[row].temp.max))°")
                            .foregroundColor(.white)
                        Text("\(self.listData[row].temp.min.toCelsiusTemp(kelvinTemp: self.listData[row].temp.min))°")
                            .foregroundColor(.white)
                    }
                    .padding(.trailing, 18)
                    
                }
                .padding()
                .listRowInsets(EdgeInsets())
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .padding(0)
        }
        .onAppear() {
            weatherViewModel.getAPI()
        }
        .listStyle(PlainListStyle())
        .background(Color(red: 0.01, green: 0.748, blue: 0.652))
    }
}
