//
//  SideMenu.swift
//  SideMenu
//
//  Created by Balaji on 05/09/21.
//

import SwiftUI

struct SideMenu: View {
    @Binding var showMenu: Bool
    var body: some View {
        
        VStack(alignment: .trailing, spacing: 0) {
            Text("My Weather")
                .fontWeight(.semibold)
                .font(.system(size: 20))
                .padding()
                .listRowInsets(EdgeInsets())
                .frame(width: UIScreen.main.bounds.width - 90, height: 44, alignment: .center)
                .foregroundColor(Color(red: 0.01, green: 0.748, blue: 0.652))

            Rectangle()
                .fill(Color(red: 0.01, green: 0.748, blue: 0.652))
                .frame(width: UIScreen.main.bounds.width - 90, height: 2, alignment: .center)
                .padding(.top, 6)
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack(spacing: 10) {
                    
                    VStack(alignment: .leading, spacing: 45) {
                        
                        // Tab Buttons...
                        TabButton(title: "Cài đặt", image: "ic_setting")
                        
                        TabButton(title: "Phản hồi", image: "ic_setting")
                        
                        TabButton(title: "Mua hàng", image: "ic_setting")
                        
                        TabButton(title: "Đánh giá", image: "ic_setting")
                        
                    }
                    .padding(.horizontal)
                    .padding(.leading)
                    .padding(.top,45)
                    
                    Divider()
                        .padding(.vertical)
                    
                }
            } .background(.white)
            
        } 
    }
    
    
    @ViewBuilder
    func TabButton(title: String,image: String) -> some View {
        
        NavigationLink {
            
            Text("\(title) View")
                .navigationTitle(title)
                .foregroundColor(Color(red: 0.01, green: 0.748, blue: 0.652))
            
        } label: {
            
            HStack(spacing: 14) {
                
                Image(image)
                    .resizable()
                    .renderingMode(.template)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 22, height: 22)
                    .foregroundColor(Color(red: 0.01, green: 0.748, blue: 0.652))
                
                Text(title)
                    .foregroundColor(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .foregroundColor(.primary)
            .frame(maxWidth: .infinity,alignment: .leading)
        }
    }
}

struct SideMenu_Previews: PreviewProvider {
    static var previews: some View {
        SideMenu(showMenu: .constant(false))
    }
}

extension View{
    func getRect() -> CGRect {
        return UIScreen.main.bounds
    }
    
    func safeArea() -> UIEdgeInsets{
        let null = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        guard let screen = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
            return null
        }
        
        guard let safeArea = screen.windows.first?.safeAreaInsets else {
            return null
        }
        
        return safeArea
    }
}
