//
//  WeatherViewModel.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import Foundation
import Combine

class WeatherViewModel: ObservableObject {
    private var weatherService: WeatherService!
    
    @Published var mainWeather = MainWeather()
    @Published var weather = Weather()
    @Published var weatherHourlyList = [HourlyWeather]()
    @Published var list8DaysWeather = [EightDayList]()
    @Published var listWeather = [Weather]()
    @Published var listDataCities = [City]()
    
    var latLocation: String = "12"
    var lonLocation: String = "23"
    var listCities: [City] {
        return self.listDataCities
    }
    
    init() {
        self.weatherService = WeatherService()
    }
    
    func getListCities() {
        self.listDataCities = weatherService.getListDataCity()
    }
    
    func getAPI() {
        if let latitude = self.latLocation.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let longitude = self.lonLocation.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            get8WeatherDay(latitude: latitude, longitude: longitude)
            getWeatherHourly(latitude: latitude, longitude: longitude)
            getWeather(latitude: latitude, longitude: longitude)
        }
    }
    
     func get8WeatherDay(latitude: String, longitude: String) {
        self.weatherService.get8WeatherDay(latitude: latitude, longitude: longitude) { response in
            if let response = response {
                DispatchQueue.main.async {
                    self.list8DaysWeather = response.list
                }
            }
        }
    }
    
     func getWeatherHourly(latitude: String, longitude: String) {
        self.weatherService.getWeatherHourly(latitude: latitude, longitude: longitude) { response in
            if let response = response {
                DispatchQueue.main.async {
                    self.weatherHourlyList = response.list
                }
            }
        }
    }
    
    func getWeather(latitude: String, longitude: String) {
        self.weatherService.getWeather(latitude: latitude, longitude: longitude) { response in
            if let response = response {
                DispatchQueue.main.async {
                    self.mainWeather = response.main
                    self.listWeather = response.weather
                }
            }
        }
    }
    
    func setupImageOfWeather(ic: String) -> String {
        switch weather.icon {
        case "01d":
            return "ic_wind"
        case "02d":
            return "ic_rain"
        case "03d":
            return "ic_humidity"
        case .none:
            return "ic_rain"
        case .some(_):
            return "ic_rain"
        }
    }
    
}


