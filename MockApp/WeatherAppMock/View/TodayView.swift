//
//  TodayView.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//


import SwiftUI

struct TodayView: View {
    @State var data: [Weather]
    @State var listDataHour: [HourlyWeather]
    @State var today: EightDayList
    
    @StateObject var weatherViewModel = WeatherViewModel()
    
    let taskDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm dd 'Tháng' MM"
        return formatter
    }()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("\(Date(), formatter: taskDateFormat)")
                    .foregroundColor(.white)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)
                
                HStack(alignment: .center, spacing: 74) {
                    if listDataHour.count > 0 {
                        ForEach(data, id: \.id) { item in
                            ItemCellView(item: item, listDataHour: today, index: 0)
                        }
                    }
                    
                }
                
                Rectangle()
                    .fill(.white)
                    .frame(width: UIScreen.main.bounds.width - 32, height: 2, alignment: .center)
                    .padding(.top, 10)
                
                Text("Hằng giờ")
                    .foregroundColor(.white)
                    .fontWeight(.heavy)
                    .font(.system(size: 18))
                    .padding()
                    .listRowInsets(EdgeInsets())
                    .frame(width: UIScreen.main.bounds.width, height: 44, alignment: .center)
                    .background(Color(red: 0.01, green: 0.748, blue: 0.652))
                
                ForEach(0..<listDataHour.count) { row in
                    HStack(alignment: .center) {
                        Text("\(listDataHour[row].hour)h")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.leading, 18)
                        Spacer()
                        Image("\(String().setImage(image: self.listDataHour[row].weather.first?.icon ?? ""))")
                            .frame(width: 20, height: 20, alignment: .center)
                        Spacer()
                        Image("ic_humidity")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text("\(listDataHour[row].main.humidity)%")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                        Spacer()
                        Image("ic_temp")
                            .frame(width: 20, height: 20, alignment: .center)
                        Text("\(listDataHour[row].main.temp.toCelsiusTemp(kelvinTemp: listDataHour[row].main.temp))")
                            .foregroundColor(.white)
                            .fontWeight(.bold)
                            .padding(.trailing, 18)
                    }
                }
                .padding()
                .listRowInsets(EdgeInsets())
                .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            }
            .background(Color(red: 0.01, green: 0.748, blue: 0.652))
            .listStyle(PlainListStyle())
        }  .background(Color(red: 0.01, green: 0.748, blue: 0.652))
    }
}

struct ItemCellView: View {
    var item: Weather
    var listDataHour: EightDayList
    var index: Int = 0
    var body: some View {
        HStack(alignment: .center, spacing: 74) {
            VStack(alignment: .center) {
                Text("\(listDataHour.temp.day.toCelsiusTemp(kelvinTemp: listDataHour.temp.day))°")
                    .foregroundColor(.white)
                    .padding(.top, 60)
                    .padding(.bottom, 30)
                    .font(.system(size: 70).bold())
                Text("Cao nhất \(listDataHour.temp.max.toCelsiusTemp(kelvinTemp: listDataHour.temp.max ))°")
                    .fontWeight(.medium)
                    .foregroundColor(.white)
                Text("Thấp nhất \(listDataHour.temp.min.toCelsiusTemp(kelvinTemp: listDataHour.temp.min ))°")
                    .fontWeight(.medium)
                    .foregroundColor(.white)
            }
            
            VStack(alignment: .center) {
                Image( "ic_rain_big")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 128, height: 128, alignment: .center)
                Text((listDataHour.weather.count > 0) ? "Mưa dông" : listDataHour.weather[index].weatherDescription)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .fontWeight(.medium)
                    .foregroundColor(.white)
            }
        }
        
    }
    
}
