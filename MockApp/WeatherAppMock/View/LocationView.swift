//
//  LocationView.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import SwiftUI

struct LocationView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var cityViewModel: CityViewModel
    
    init() {
        self.cityViewModel = CityViewModel()
        self.cityViewModel.getListCities()
        UITableView.appearance().separatorStyle = .none
        UITableViewCell.appearance().backgroundColor = UIColor(red: 0.01, green: 0.748, blue: 0.652, alpha: 1)
        UITableView.appearance().backgroundColor = UIColor(red: 0.01, green: 0.748, blue: 0.652, alpha: 1)
    }
    
    var body: some View {
        VStack(alignment: .center) {
            HStack(alignment: .center) {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                        .padding(.leading, 18)
                }
                Spacer()
                Text("Địa điểm")
                    .font(.system(size: 18))
                    .foregroundColor(.white)
                    .fontWeight(.heavy)
                Spacer()
            }
            .padding(.top, 44)
            .padding(.bottom, 40)
            ScrollView {
                ForEach(0..<self.cityViewModel.listCities.count) { row in
                    Button(action: {
                        let userInfo = ["lat": self.cityViewModel.listCities[row].lat ?? "",
                                        "lon": self.cityViewModel.listCities[row].lon ?? "",
                                        "name": self.cityViewModel.listCities[row].name ?? ""]
                        NotificationCenter.default.post(name: .invokeSelectCity, object: nil, userInfo: userInfo)
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("\(self.cityViewModel.listCities[row].name ?? ""), \(self.cityViewModel.listCities[row].country ?? "")")
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                            .font(.system(size: 16))
                            .padding()
                            .overlay(
                                RoundedRectangle(cornerRadius: 16)
                                    .stroke(.white, lineWidth: 2)
                                    .frame(width: UIScreen.main.bounds.width - 36, height: 42, alignment: .center)
                            )
                            .frame(width: UIScreen.main.bounds.width, height: 42, alignment: .center)
                    }.padding(.bottom, 23)
                        .padding(.top, 1)
                }
            }.background(.clear)
            Spacer()
        }
        .navigationBarHidden(true)
        .frame(minWidth: 0, idealWidth: .greatestFiniteMagnitude, maxWidth: .infinity, minHeight: 0, idealHeight: .greatestFiniteMagnitude, maxHeight: .infinity, alignment: .center)
        .background(Color(red: 0.01, green: 0.748, blue: 0.652))
        .ignoresSafeArea()
    }
}

struct LocationView_Previews: PreviewProvider {
    static var previews: some View {
        LocationView()
    }
}
