//
//  Constant.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/10/2022.
//

import Foundation

class Constant {
    static let API_KEY: String = "67105490a2d1e47666bca7fe4a48b214"
    static func urlWeatherByCoordinate(lat: String, lon: String) -> URL? {
        return URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(Constant.API_KEY)&units=metric&lang=vi")
    }
    static func urlWeatherHourlyByCoordinate(lat: String, lon: String) -> URL? {
        return URL(string: "https://api.openweathermap.org/data/2.5/forecast/hourly?lat=\(lat)&lon=\(lon)&appid=\(Constant.API_KEY)&units=metric&lang=vi")
    }
    static func url8WeatherDayByCoordinate(lat: String, lon: String) -> URL? {
        return URL(string: "https://api.openweathermap.org/data/2.5/forecast/daily?lat=\(lat)&lon=\(lon)&cnt=8&appid=\(Constant.API_KEY)&units=metric&lang=vi")
    }
}

extension String {
    func timeFormatyyyyMMddHHmmss(withFormat format: String = "yyyy-MM-dd HH:mm:ss") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension Date {
    func dateAfterNumberOfdays(days: Int) -> Date {
        Calendar.current.date(byAdding: .day, value: days, to: Date()) ?? Date()
    }
}

extension NSNotification.Name {
    static let invokeSelectCity = Notification.Name("invokeSelectCity")
}

extension String {
    func setImage(image: String) -> String {
        switch image {
        case "a":
            return "ic_humidity"
        case "b":
            return "ic_rain"
        case "c":
            return "ic_wind"
        case "d":
            return "ic_temp"
        default:
            return ""
        }
    }
}

extension Double {
    func toCelsiusTemp(kelvinTemp: Double) -> String {
        let celsiusTemp = kelvinTemp - 273.15
        return String(format: "%.0f", celsiusTemp)
    }
}

extension KeyedEncodingContainerProtocol {

    public mutating func encodeArray<T>(_ values: [T], forKey key: Self.Key) throws where T : Encodable {
        var arrayContainer = nestedUnkeyedContainer(forKey: key)
        try arrayContainer.encode(contentsOf: values)
    }

    public mutating func encodeArrayIfPresent<T>(_ values: [T]?, forKey key: Self.Key) throws where T : Encodable {
        if let values = values {
            try encodeArray(values, forKey: key)
        }
    }

    public mutating func encodeMap<T>(_ pairs: [Self.Key: T]) throws where T : Encodable {
        for (key, value) in pairs {
            try encode(value, forKey: key)
        }
    }

    public mutating func encodeMapIfPresent<T>(_ pairs: [Self.Key: T]?) throws where T : Encodable {
        if let pairs = pairs {
            try encodeMap(pairs)
        }
    }

}

extension KeyedDecodingContainerProtocol {

    public func decodeArray<T>(_ type: T.Type, forKey key: Self.Key) throws -> [T] where T : Decodable {
        var tmpArray = [T]()
        var nestedContainer: Any?
        do {
            nestedContainer = try nestedUnkeyedContainer(forKey: key)
        }
        catch {
            return tmpArray
        }
        var nestedContainerUnkeyedDecoding = nestedContainer as! UnkeyedDecodingContainer
        while !nestedContainerUnkeyedDecoding.isAtEnd {
            let arrayValue = try nestedContainerUnkeyedDecoding.decode(T.self)
            tmpArray.append(arrayValue)
        }

        return tmpArray
    }

    public func decodeArrayIfPresent<T>(_ type: T.Type, forKey key: Self.Key) throws -> [T]? where T : Decodable {
        var tmpArray: [T]? = nil

        if contains(key) {
            tmpArray = try decodeArray(T.self, forKey: key)
        }

        return tmpArray
    }

    public func decodeMap<T>(_ type: T.Type, excludedKeys: Set<Self.Key>) throws -> [Self.Key: T] where T : Decodable {
        var map: [Self.Key : T] = [:]

        for key in allKeys {
            if !excludedKeys.contains(key) {
                let value = try decode(T.self, forKey: key)
                map[key] = value
            }
        }

        return map
    }

}
