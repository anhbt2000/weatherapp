//
//  MainHomeView.swift
//  WeatherAppMock
//
//  Created by MR932 on 24/08/2022.
//

import SwiftUI

struct MainHomeView: View {
    
    @State private var selectedTab : Int = 0
    @State private var lat : String = "12"
    @State private var lon : String = "23"
    @State private var name : String = "Ha Noi"
    @State private var isPush = false
    @State private var isShowMenu: Bool = false
    @State var showMenu: Bool = false
    // Offset for Both Drag Gesuture and showing Menu...
    @State var offset: CGFloat = 0
    @State var lastStoredOffset: CGFloat = 0
    
    // GEsture Offset...
    @GestureState var gestureOffset: CGFloat = 0
    
    @StateObject var weatherViewModel = WeatherViewModel()
    @State var locationManager = LocationManager()
    
    var userLatitude: String {
        return "\(locationManager.myLocation?.coordinate.latitude ?? 0)"
    }
    
    var userLongitude: String {
        return "\(locationManager.myLocation?.coordinate.longitude ?? 0)"
    }
    var body: some View {
        let sideBarWidth = getRect().width - 90
        VStack {
            ZStack {
                HStack(spacing: 0) {
                    // Side Menu...
                    SideMenu(showMenu: $showMenu)
                    
                    VStack(spacing: 0) {
                        HStack(alignment: .center) {
                            Image("ic_menu")
                                .padding(.leading, 18)
                                .onTapGesture {
                                    showMenu.toggle()
                                }
                            Spacer()
                            
                            HStack(alignment: .center, spacing: 0) {
                                Spacer()
                                Image("ic_location")
                                    .padding(.trailing, 10.0)
                                Text(self.name)
                                    .foregroundColor(.white)
                                    .fontWeight(.heavy)
                                Spacer()
                            }
                            Spacer()
                            
                            NavigationLink("", isActive: $isPush, destination: {
                                LocationView()
                            })
                            
                            Button(action: {
                                isPush = true
                            }) {
                                Image("ic_search")
                                    .padding(.trailing, 18)
                            }
                            
                        }.padding(.top, 44)
                            .padding(.bottom, 34)
                        
                        HStack(alignment: .center) {
                            TabBarView(selectedTab: $selectedTab, tabBarName: ["HÔM NAY", "NGÀY MAI", "8 NGÀY"])
                        } .padding(.top, 0)
                        
                        ZStack {
                            Rectangle().fill(Color.clear)
                            
                            switch selectedTab {
                            case 0:
                                if weatherViewModel.listWeather.count > 0 && weatherViewModel.list8DaysWeather.count > 0 {
                                    TodayView(data: weatherViewModel.listWeather, listDataHour: weatherViewModel.weatherHourlyList, today: weatherViewModel.list8DaysWeather[0])
                                        .background(Color.white)
                                        .transition(.identity)
                                        .onAppear() {
                                            weatherViewModel.getAPI()
                                        }
                                }
                                
                            case 1:
                                if weatherViewModel.listWeather.count > 0 && weatherViewModel.list8DaysWeather.count > 0 {
                                    TomorowDay(data: weatherViewModel.listWeather, listDataHour: weatherViewModel.weatherHourlyList, tomorow: weatherViewModel.list8DaysWeather[1])
                                        .background(Color.white)
                                        .transition(.identity)
                                }
                            case 2:
                                
                                if weatherViewModel.list8DaysWeather.count > 0 {
                                    EightDaysView(listData: weatherViewModel.list8DaysWeather)
                                        .background(Color.white)
                                        .transition(.identity)
                                }
                            default:
                                if weatherViewModel.weatherHourlyList.count > 0 {
                                    TodayView(data: weatherViewModel.listWeather, listDataHour: weatherViewModel.weatherHourlyList, today: weatherViewModel.list8DaysWeather[0])
                                        .background(Color.white)
                                        .transition(.identity)
                                }
                            }
                            
                            
                        }
                        
                        Spacer()
                    }
                    .navigationBarHidden(true)
                    .frame(minWidth: 0, idealWidth: .greatestFiniteMagnitude, maxWidth: .infinity, minHeight: 0, idealHeight: .greatestFiniteMagnitude, maxHeight: .infinity, alignment: .center)
                    .background(Color(red: 0.01, green: 0.748, blue: 0.652))
                    .edgesIgnoringSafeArea(.all)
                    .frame(width: getRect().width)
                    .overlay(
                        
                        Rectangle()
                            .fill(
                                
                                Color.primary
                                    .opacity(Double((offset / sideBarWidth) / 5))
                            )
                            .ignoresSafeArea(.container, edges: .vertical)
                            .onTapGesture {
                                withAnimation{
                                    showMenu.toggle()
                                }
                            }
                    )
                }
                .frame(width: getRect().width + sideBarWidth)
                .offset(x: -sideBarWidth / 2)
                .offset(x: offset > 0 ? offset : 0)
                // Gesture...
                .gesture(
                    
                    DragGesture()
                        .updating($gestureOffset, body: { value, out, _ in
                            out = value.translation.width
                        })
                        .onEnded(onEnd(value:))
                )
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarHidden(true)
            }
            .animation(.easeOut, value: offset == 0)
            .onChange(of: showMenu) { newValue in
                if showMenu && offset == 0 {
                    offset = sideBarWidth
                    lastStoredOffset = offset
                }
                
                if !showMenu && offset == sideBarWidth {
                    offset = 0
                    lastStoredOffset = 0
                }
            }
            .onChange(of: gestureOffset) { newValue in
                onChange()
            }
        }
        
        .onAppear() {
            //call API
            NotificationCenter.default.addObserver(forName: .invokeSelectCity, object: nil, queue: .main) { [self] noti in
                guard let lat = noti.userInfo?["lat"] as? String , let lon = noti.userInfo?["lon"] as? String, let name = noti.userInfo?["name"] as? String else { return }
                weatherViewModel.latLocation = lat
                weatherViewModel.lonLocation = lon
                self.name = name
                weatherViewModel.getAPI()
                return
            }
            lat = userLatitude
            lon = userLongitude
            weatherViewModel.getAPI()
        }
    }
    
    func onChange() {
        let sideBarWidth = getRect().width - 90
        
        offset = (gestureOffset != 0) ? ((gestureOffset + lastStoredOffset) < sideBarWidth ? (gestureOffset + lastStoredOffset) : offset) : offset
        
        offset = (gestureOffset + lastStoredOffset) > 0 ? offset : 0
    }
    
    func onEnd(value: DragGesture.Value) {
        
        let sideBarWidth = getRect().width - 90
        
        let translation = value.translation.width
        
        withAnimation {
            // Checking...
            if translation > 0 {
                
                if translation > (sideBarWidth / 2) {
                    // showing menu...
                    offset = sideBarWidth
                    showMenu = true
                }
                else {
                    if offset == sideBarWidth || showMenu {
                        return
                    }
                    offset = 0
                    showMenu = false
                }
            }
            else{
                
                if -translation > (sideBarWidth / 2) {
                    offset = 0
                    showMenu = false
                } else {
                    if offset == 0 || !showMenu{
                        return
                    }
                    
                    offset = sideBarWidth
                    showMenu = true
                }
            }
        }
        
        // storing last offset...
        lastStoredOffset = offset
    }
    
}

struct MainHomeView_Previews: PreviewProvider {
    static var previews: some View {
        MainHomeView()
    }
}

