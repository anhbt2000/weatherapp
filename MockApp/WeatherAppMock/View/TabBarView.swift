//
//  TabBarView.swift
//  WeatherAppMock
//
//  Created by MR932 on 25/08/2022.
//

import SwiftUI

struct TabBarView: View {
    @Binding var selectedTab: Int
    var tabBarName: [String]
    let color = Color.red
    
    var body: some View {
        HStack(spacing: 0) {
            ForEach(tabBarName.indices, id:\.self) { index in
                let isSelected = (selectedTab == index)
                ZStack {
                    Rectangle()
                        .fill(Color(red: 0.01, green: 0.748, blue: 0.652))
                    
                    Rectangle()
                        .fill(Color(red: 0.01, green: 0.748, blue: 0.652))
                        .onTapGesture {
                            withAnimation {
                                selectedTab = index
                            }
                        }
                }
                .overlay(
                    VStack(alignment: .center) {
                        Text(tabBarName[index])
                            .foregroundColor(.white)
                            .fontWeight(.medium)
                        Rectangle()
                            .fill(isSelected ? .white : .clear)
                    }
                )
            }
        }
        .frame(height: 40)
    }
}
